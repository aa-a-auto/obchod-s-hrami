
# :video_game: Obchod s hrami

Závěrečný projekt do SSP
NEV0067 :muscle: :boar: & BIE0054 :muscle: :boar:

## :ski: Téma projektu

Jedná se o testovací verzi internetového rozhraní obchodu s hrami
(kde figurují uživatelé, položky, slevy, vydavatelé,...)

Jelikož se jedná o dynamickou stránku,
která pracuje s požadavky uživatele,
pro úplné testování je třeba ji stáhnout
(jelikož GitLab pages hostuje pouze statické stránky)

## :snowboarder: Použití

```bash
#Přesun do adresáře projektu
cd adresarProjektu

#Spuštění virtuálního prostředí
source adresarProjektu/venv/bin/activate

#Spuštění serveru
python3 adresarProjektu/manage.py runserver
```
