"""obchodSHrami URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from obchodSHramiApp import views

admin.site.site_header = "Obchod s hrami"
admin.site.site_title = "Obchod s hrami - NEV0067"
admin.site.index_title = "Admin konzole"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('register/', views.register, name='register'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('documentation/', views.documentation, name='documentation'),

    path('items/', views.items, name='items'),
    path('categories/', views.categories, name='categories'),
    path('publishers/', views.publishers, name='publishers'),
    path('sales/', views.sales, name='sales'),

    path('editItems/<int:itemId>', views.editItems, name='editItems'),
    path('editCategories/<int:categoryId>', views.editCategories, name='editCategories'),
    path('editPublishers/<int:publisherId>', views.editPublishers, name='editPublishers'),
    path('editSales/<int:saleId>', views.editSales, name='editSales')
]