# Generated by Django 4.0.4 on 2022-05-18 20:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('obchodSHramiApp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='uzivatele',
            name='heslo',
        ),
        migrations.RemoveField(
            model_name='uzivatele',
            name='jmeno',
        ),
        migrations.RemoveField(
            model_name='uzivatele',
            name='prijmeni',
        ),
    ]
