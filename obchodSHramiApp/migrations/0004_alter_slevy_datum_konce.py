# Generated by Django 4.0.4 on 2022-05-18 22:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('obchodSHramiApp', '0003_alter_polozky_polozky_id_polozky'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slevy',
            name='datum_konce',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
