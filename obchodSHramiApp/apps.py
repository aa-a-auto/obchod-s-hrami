from django.apps import AppConfig


class ObchodshramiappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'obchodSHramiApp'
