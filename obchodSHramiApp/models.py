from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.contrib.auth.validators import ASCIIUsernameValidator

# Create your models here.

class Polozky(models.Model):
    nazev = models.CharField(max_length=50)
    popis = models.CharField(max_length=200)
    cena = models.IntegerField()
    ks_momentalne = models.IntegerField()
    ks_min = models.IntegerField()
    Kategorie_id_kategorie = models.ForeignKey('Kategorie', on_delete=models.CASCADE)
    Vydavatele_id_vydavatele = models.ForeignKey('Vydavatele', on_delete=models.CASCADE)
    Polozky_id_polozky = models.ForeignKey('Polozky', on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.nazev

    @property
    def Property_cena(self):
        return f"{self.cena} €"

class Kategorie(models.Model):
    nazev = models.CharField(max_length=50)
    popis = models.CharField(max_length=200)
    
    def __str__(self):
        return self.nazev

class Vydavatele(models.Model):
    nazev = models.CharField(max_length=50)
    popis = models.CharField(max_length=200)

    def __str__(self):
        return self.nazev

class Slevy(models.Model):
    Polozky_id_polozky = models.ForeignKey('Polozky', on_delete=models.CASCADE)
    datum_pocatku = models.DateTimeField()
    datum_konce = models.DateTimeField(blank=True, null=True)
    pomer_slevy = models.FloatField()
    stav_slevy = models.BooleanField()

    def __str__(self):
        return f'Sleva polozky: {self.Polozky_id_polozky}'

    @property
    def Property_vyse(self):
        return f"{self.pomer_slevy*100} %"

    @property
    def Property_stav(self):
        if self.stav_slevy:
            return 'Aktivní'
        else:
            return 'Neaktivní'

class polozky_nakupy(models.Model):
    Polozky_id_polozky = models.ForeignKey('Polozky', on_delete=models.CASCADE)
    Nakupy_id_nakupu = models.ForeignKey('Nakupy', on_delete=models.CASCADE)
    cena = models.IntegerField()

    def __str__(self):
        return f'Polozka: {self.Polozky_id_polozky}, Nakup: {self.Nakupy_id_nakupu}, Cena: {self.cena}'

class Nakupy(models.Model):
    Polozky_id_polozky = models.ManyToManyField('Polozky', through='polozky_nakupy')
    datum = models.DateTimeField()
    cas = models.TimeField()
    cena = models.IntegerField()
    Uzivatele_id_kupujiciho = models.ForeignKey('Uzivatele', on_delete=models.CASCADE)

    def __str__(self):
        return f'Kupujici: {self.Uzivatele_id_kupujiciho}, Datum: {self.datum}, Cas: {self.cas}'

class Uzivatele(AbstractUser, PermissionsMixin):
    username_validator = ASCIIUsernameValidator()

    username = models.CharField(max_length=50, unique=True, validators=[username_validator], error_messages={"unique": ("A user with that username already exists."),},)
    email = models.EmailField(max_length=50, unique=True, error_messages={"unique": ("A user with that email address already exists."),},)

    def __str__(self):
        return self.username