from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Uzivatele, Polozky, Kategorie, Vydavatele, Slevy, polozky_nakupy, Nakupy

# Register your models here.

admin.site.register(Uzivatele, UserAdmin)
admin.site.register(Polozky)
admin.site.register(Kategorie)
admin.site.register(Vydavatele)
admin.site.register(Slevy)
admin.site.register(polozky_nakupy)
admin.site.register(Nakupy)