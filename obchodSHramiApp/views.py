from django.shortcuts import render, redirect
from obchodSHramiApp.forms import RegisterForm, EditItemsForm, EditCategoriesForm, EditPublishersForm, EditSalesForm
from obchodSHramiApp.models import Polozky, Kategorie, Vydavatele, Slevy
from django.contrib.auth import authenticate, login as djangoLogin, logout as djangoLogout
from django.contrib import messages

# Create your views here.

def index(request):
    return render(request, 'index.html', {})

def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            djangoLogin(request, user)
            return redirect('index')
        messages.error(request, "Unsuccessful registration. Invalid information.")
    form = RegisterForm()
    return render(request, 'register.html', {})

def login(request):
    if request.method == 'POST':
        tempUser = authenticate(username=request.POST["username"], password=request.POST["password"])
        if tempUser is not None:
            djangoLogin(request, tempUser)
            return redirect('index')
    return render(request, 'login.html', {})

def logout(request):
    djangoLogout(request)
    return redirect('index')

def documentation(request):
    return render(request, 'documentation.html', {})

def items(request):
    if request.user.is_authenticated:
        itemList = Polozky.objects.all()
        return render(request, 'items.html', {'itemList': itemList})
    else:
        return redirect('login')

def categories(request):
    if request.user.is_authenticated:
        categoryList = Kategorie.objects.all()
        return render(request, 'categories.html', {'categoryList': categoryList})
    else:
        return redirect('login')

def publishers(request):
    if request.user.is_authenticated:
        publisherList = Vydavatele.objects.all()
        return render(request, 'publishers.html', {'publisherList': publisherList})
    else:
        return redirect('login')

def sales(request):
    if request.user.is_authenticated:
        saleList = Slevy.objects.all()
        return render(request, 'sales.html', {'saleList': saleList})
    else:
        return redirect('login')

def editItems(request, itemId):
    requestedItem = Polozky.objects.get(id = itemId)
    if request.method == 'POST':
        form = EditItemsForm(request.POST, instance = requestedItem)
        if form.is_valid():
            form.save()
            return redirect('items')
    form = EditItemsForm(instance = requestedItem)
    return render(request, 'editItems.html', {'form' : form, 'itemId' : itemId})

def editCategories(request, categoryId):
    requestedCategory = Kategorie.objects.get(id = categoryId)
    if request.method == 'POST':
        form = EditCategoriesForm(request.POST, instance = requestedCategory)
        if form.is_valid():
            form.save()
            return redirect('categories')
    form = EditCategoriesForm(instance = requestedCategory)
    return render(request, 'editCategories.html', {'form' : form, 'categoryId' : categoryId})

def editPublishers(request, publisherId):
    requestedPublisher = Vydavatele.objects.get(id = publisherId)
    if request.method == 'POST':
        form = EditPublishersForm(request.POST, instance = requestedPublisher)
        if form.is_valid():
            form.save()
            return redirect('publishers')
    form = EditPublishersForm(instance = requestedPublisher)
    return render(request, 'editPublishers.html', {'form' : form, 'publisherId' : publisherId})

def editSales(request, saleId):
    requestedSale = Slevy.objects.get(id = saleId)
    if request.method == 'POST':
        form = EditSalesForm(request.POST, instance = requestedSale)
        if form.is_valid():
            form.save()
            return redirect('sales')
    form = EditSalesForm(instance = requestedSale)
    return render(request, 'editSales.html', {'form' : form, 'saleId' : saleId})
