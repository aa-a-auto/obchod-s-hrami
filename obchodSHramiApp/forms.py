from django.forms import ModelForm
from obchodSHramiApp.models import Uzivatele, Polozky, Kategorie, Vydavatele, Kategorie, Slevy

class RegisterForm(ModelForm):
    class Meta:
        model = Uzivatele
        fields = ['username', 'password', 'email']

class EditItemsForm(ModelForm):
    class Meta:
        model = Polozky
        exclude = ['Polozky_id_polozky']
        labels = {
            'nazev': 'Název',
            'popis': 'Popis',
            'cena': 'Cena (v €)',
            'ks_momentalne': 'Kusů momentálně',
            'ks_min': 'Kusů minimálně',
            'Kategorie_id_kategorie': 'Kategorie',
            'Vydavatele_id_vydavatele': 'Vydavatel'
        }

class EditCategoriesForm(ModelForm):
    class Meta:
        model = Kategorie
        exclude = []
        labels = {
            'nazev' : 'Název',
            'popis' : 'Popis'
        }

class EditPublishersForm(ModelForm):
    class Meta:
        model = Vydavatele
        exclude = []
        labels = {
            'nazev' : 'Název',
            'popis' : 'Popis'
        }

class EditSalesForm(ModelForm):
    class Meta:
        model = Slevy
        exclude = []
        labels = {
            'Polozky_id_polozky' : 'Zlevněná položka',
            'datum_pocatku' : 'Datum začátku',
            'datum_konce' : 'Datum konce',
            'pomer_slevy' : 'Poměr slevy',
            'stav_slevy' : 'Stav slevy'
        }
        